var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer')
    // bless = require('gulp-bless'),
    data = require('gulp-data'),
    del = require('del'),
    file_utilities = require('./lib/file_utilities'),
    neat = require('node-neat').includePaths,
    nunjucks = require('gulp-nunjucks'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass');

var src = {
    root: 'src/',
    scss: 'src/scss/',
    css: 'src/css/',
    html: 'src/html/',
    fonts: 'src/fonts/',
    js: 'src/js/'
};

var distribution = {
    root: 'dist/',
    scss: 'dist/scss/',
    img: 'dist/img/',
    css: 'dist/css/',
    html: 'dist/html/',
    fonts: 'dist/fonts/',
    js: 'dist/js/',
    libs: 'dist/js/libs/'
};

gulp.task('clean', function(){
    return del.sync([distribution.root]);
});

gulp.task('js', function() {
    return gulp.src(src.js + '**/*.js')
        .pipe(gulp.dest(distribution.js));
});

gulp.task('scss', function () {
  return gulp.src(src.scss + '**/*.scss')
    .pipe(plumber())
    .pipe(sass({
        includePaths: ['styles'].concat(neat),
    }))
    .pipe(autoprefixer('last 5 version'))
    // .pipe(bless())
    .pipe(gulp.dest(distribution.css));
});

gulp.task('html',function(){
    opts = {
        extension: 'html',
        exclude: 'all.html',
        postprocessor: file_utilities.basename
    }

    var atoms = file_utilities.getFilesInFolder('src/html/atoms/', opts);
    var molecules = file_utilities.getFilesInFolder('src/html/molecules/', opts);
    var organisms = file_utilities.getFilesInFolder('src/html/organisms/', opts);
    console.log(organisms);
    return gulp.src(src.html + '*.html')
        .pipe(data(() => ({atoms: atoms, molecules: molecules, organisms: organisms})))
        .pipe(nunjucks.compile())
        .pipe(gulp.dest(distribution.html));
});

gulp.task('watch', function () {
    gulp.watch(src.js + "**/*.js", ['js']);
    gulp.watch(src.scss + "**/*.scss", ['scss']);
    gulp.watch(src.html + "**/*.html", ['html']);
});

gulp.task('dist', ['scss', 'html', 'js']);