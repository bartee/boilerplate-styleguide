/**
* Created by bartstroeken on 09/04/14.
*/
var tabcollection = function(options){
    defaults = {
        classnames: {
            active: 'btn-danger',
            passive: 'btn-default'
        },
        prefixes: {
            container: '#fieldset_'
        }  
    }
    self = this;
    self.options = jQuery.extend(defaults,options);
    self.tabs = jQuery(options.selectors.tabs);
    self.contents = jQuery(options.selectors.containers);

    self.tabs.on('click', function(){tcol.show(jQuery(this));});
}

tabcollection.prototype.hideall = function() {
    self.contents.hide();
    self.tabs.removeClass(self.options.classnames.active).addClass(self.options.classnames.passive);
}

tabcollection.prototype.show = function (el){
    self.hideall();
    var id = el.attr('id');
    el.removeClass(self.options.classnames.passive).addClass(self.options.classnames.active);
    jQuery(self.options.prefixes.container + id).show();
}