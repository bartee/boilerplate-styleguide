var expect = require("chai").expect;
var fs = require('fs');
var file_utilities = require('../lib/file_utilities')

describe('File Utilities test', function(){
    // Strip a filename

    it("return the basename of a file", function(){

        basename = file_utilities.basename('testfile.txt')
        complex_basename = file_utilities.basename('this-is-a.complex.file.name.with.a.lot.of.dots.txt')
        
        expect(basename).equal('testfile')
        expect(complex_basename).equal('this-is-a.complex.file.name.with.a.lot.of.dots')

    })

    it('should read the content of the files in a directory into an array',function(){
        // plain reader
        var plain = file_utilities.getFilesInFolder('tests/testdir/', {});
        var extensions = file_utilities.getFilesInFolder('tests/testdir/', { extension: 'txt'});
        var filtered = file_utilities.getFilesInFolder('tests/testdir/', { exclude: 'file_b.txt'});
        var callbacked = file_utilities.getFilesInFolder('tests/testdir/', { postprocessor: file_utilities.basename});
        
        expect(plain).to.have.all.members(['file_a.txt', 'file_b.txt', 'file_c.info']);

        expect(extensions).to.have.all.members(['file_a.txt', 'file_b.txt']);
        expect(extensions).to.not.have.all.members(['file_c.info']);

        expect(filtered).to.have.all.members(['file_a.txt', 'file_c.info']);
        expect(filtered).to.not.have.all.members(['file_b.txt']);

        expect(callbacked).to.have.all.members(['file_a', 'file_b', 'file_c']);
    })
})
