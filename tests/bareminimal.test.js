var expect = require("chai").expect;
var fs = require('fs');

/**
 * A little utilitarian function to check if a file exists
 **/
function existsSync(filePath){
  try{
    fs.statSync(filePath);
  }catch(err){
    if(err.code == 'ENOENT') return false;
  }
  return true;
};

// Create a new test suite to test if the minimal required files exist
describe("Bare Minimal File Tests", function() {
    
    it('Should contain a few files', function(){
        
        // Expect a couple of files to exist
        minimal_files = [
            'src/html/atoms/all.html',
            'src/html/molecules/all.html',
            'src/html/organisms/all.html',
            'src/scss/_styleguide_container.scss'
        ];

        for (index in minimal_files){
            expect(existsSync(minimal_files[index])).to.be.ok;
        }
    })
    
    
});