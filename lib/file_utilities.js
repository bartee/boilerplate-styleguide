var fs = require('fs'),
    path = require('path');

module.exports = {
    /**
     * Get the files in a folder
     * @param dir foldername
     * @param opts Options:
     *  opts.extension: file extension to retrieve
     *  opts.exclude: file to exclude
     **/
    getFilesInFolder: function (dir, opts) {
        try {
            result = fs.readdirSync(dir).filter(function(file) {
                    var filename = file;
                    fullpath = path.join(dir, filename);
                    if (fs.lstatSync(fullpath).isFile()){
                        valid = true;
                        if (opts.extension){
                            valid = false;
                            if (filename.split('.').pop() == opts.extension){
                                valid = true;
                            }
                        }
                        if (opts.exclude){
                            valid = false;
                            // @TODO opts.exclude could be an array
                            if (file != opts.exclude){
                                valid = true;
                            }
                        }
                        if (valid){
                            var res = file;
                            return res;
                        }
                    }
                });
            if (opts.postprocessor){
                var processed = [];
                for (i in result){
                    processed.push(opts.postprocessor(result[i]));
                }
                return processed;
            } else {
                return result;
            }
        } catch (ex) {
            console.error(dir + ' does not exist');
            return [];
        }
    },

    /**
     * Split the extension of the filename and return the rest
     * 
     * @param name filename
    **/
    basename: function(name){
        base = new String(name);
        return base.substring(0, base.lastIndexOf("."));
    }
};
