install:
	# To be called from deployment server 
	npm install
	node_modules/gulp/bin/gulp.js dist

display: install
	-node_modules/gulp/bin/gulp.js clean
	-node_modules/gulp/bin/gulp.js html
	-node_modules/gulp/bin/gulp.js sass
	-node_modules/gulp/bin/gulp.js js

test: 
	# Using mocha to run the test
	@NODE_ENV=tests ./node_modules/.bin/mocha tests --reporter=$(REPORTER)

test-w:
	# Continuous testing using mocha 
	@NODE_ENV=tests ./node_modules/.bin/mocha tests --reporter=$(REPORTER) --watch --growl

watch: install
	# Watching changes to appear... hold yer horses!
	./node_modules/.bin/gulp watch

