Boilerplate Atomic Styleguide System
====================================

This project functions as an engine for you to develop a styleguide for your project. It's based on SCSS, with Gulp as its engine. 
I've been using it throughout various projects, and I liked it so much that I created this little node toolkit for it. It will help you to create your own styleguide. And, eventually, I hope to create single set of static files to use within the actual project the styleguide's intended for. 

Getting started
===============

First off
---------
* Fork this repo, and check it out
* ```make install ``` installs the node-dependencies
* ```make display ``` cleans out the ```dist``` folder, renders the html, the css and the js

Viewing and watching
--------------------
* Go to the ```dist``` folder, and run ```python -m SingleHTTPServer```
* Open [this link](http://localhost:8000/html/index.html) in your browser to view the html 
* There's a ```make watch``` as well. That's an alias for node_modules/.bin/gulp watch. It watches changes in css, html and js, and on change, it will renew these files in the ```dist```-folder.

Vision and Background
=====================

*Why would you want something like this? You can just create html and css within your own project?*
Yeah, you could. But, you'll end up repeating yourself. You'll end up creating inconsistencies. You'll end up running through a maze of code of what's where. I prefer a systematic approach. One that you can use as a reference. 

Styleguide
----------
A design is never a single page. It's never a single form. It will all stick together. By creating a styleguide, you *have* to look at the bigger picture. 

Atomic design
-------------
Atomic design describes the chemistry of UI design in code. It translates *from* chemistry. A molecule consists of atoms, while an organism consists of molecules. 
And a template is a set of organisms. If you come to think of it, a graphic design of - let's say a product sheet, can translate into atoms of titles, images, paragraphs and - what have you. 
Do you think this sounds confusing? [This explanation did the trick for me.](http://patternlab.io/about.html#atoms)

SCSS
----
Don't repeat yourself, is an often heard credo. But, when writing plain CSS, you'll have to. SCSS lets you add magic in it. Lets you define variables, lets you use mixins to automate processes. 
There is [the sass way](http://www.sass-lang.com), which scss extends. Have a look at [this comparison](http://thesassway.com/editorial/sass-vs-scss-which-syntax-is-better), which made me decide to add scss. Although, the article states that the choice is mostly a matter of taste, [reason #5](http://thesassway.com/editorial/sass-vs-scss-which-syntax-is-better#reason-5-existing-css-tools-often-work-with-scss) and [reason #6](http://thesassway.com/editorial/sass-vs-scss-which-syntax-is-better#reason-6-integration-with-an-existing-css-codebase-is-much-easier) mentioned in the section **pro-scss** made me decide.

Eyes on the prize
-----------------
I want to be able to integrate it completely in an active repository by using a build server and a combination of hooks.

[ ![Codeship Status for bartee/boilerplate-styleguide](https://codeship.com/projects/ac5979c0-084b-0134-bae1-0e11c5137ec3/status?branch=master)](https://codeship.com/projects/154995)